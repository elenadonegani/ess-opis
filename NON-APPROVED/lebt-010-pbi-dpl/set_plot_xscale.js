importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

try {
	var startx = PVUtil.getDouble(pvs[0]);
	var sizex = PVUtil.getDouble(pvs[1]);
	var minwl = PVUtil.getDouble(pvs[2]);
	var maxwl = PVUtil.getDouble(pvs[3]);
	
	var slope = (maxwl - minwl) / 1023;
	var minxwl = startx * slope + minwl;
	var maxxwl = (startx + sizex - 1) * slope + minwl;
	
	widget.setPropertyValue("axis_2_minimum", minxwl);
	widget.setPropertyValue("axis_2_maximum", maxxwl);
} catch (error) {
    // ConsoleUtil.writeInfo("ERROR:" + error);
}
