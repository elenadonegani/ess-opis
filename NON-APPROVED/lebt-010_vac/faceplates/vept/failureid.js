PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;
logger = org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger();

var msg = "N/A";

try
{
	var error_code = PVUtil.getLong(pvs[1]);

	if (PVUtil.getLong(pvs[0]) != 2)
		switch (error_code)
		{
			case 0:
				error_code = 0;
				break;

			case 1:
				error_code = 101;
				break;

			case 3:
				error_code = 103;
				break;

			case 4:
				error_code = 5;
				break;

			case 6:
				error_code = 106;
				break;

			case 7:
				error_code = 6;
				break;

			case 8:
				error_code = 8;
				break;

			case 10:
				error_code = 3;
				break;

			case 16:
				error_code = 116;
				break;

			case 17:
				error_code = 117;
				break;

			case 19:
				error_code = 2;
				break;

			case 43:
				error_code = 143;
				break;

			case 60:
				error_code = 4;
				break;

			case 62:
				error_code = 62;
				break;

			default:
				if (error_code > 100)
					error_code += 100;
				break;
		}

	switch (error_code)
	{
		case   0:
			msg = "No error";
			break;

		case 101:
			msg = "Overload warning\nP3 < P25*P24 (after normal operation has been attained) (not during generator operation!).";
			break;

		case 103:
			msg = "Power supply voltage error\nPower supply voltage failure during active pump operation.";
			break;

		case   5:
			msg = "Converter temperature error\nP11 > limit threshold converter temperature.";
			break;

		case 106:
			msg = "Overload error\nP3 < P20 after normal operation was attained.";
			break;

		case   6:
			msg = "Run-up time error\nP3 < P24*P25 after P32 has elapsed with start signal being present.";
			break;

		case   8:
			msg = "Pump error\nPump could not be identified or no pump has been connected.";
			break;

		case   3:
			msg = "Pump temperature error\nP127 > P132 or temperature switch = ∞";
			break;

		case 116:
			msg = "Overload duration error\nP3 < P25*P24 longer of than P32.";
			break;

		case 117:
			msg = "Motor current error\nNo motor current or motor current too low.";
			break;

		case   2:
			msg = "Pass-through time error\n60 Hz < P3 < P20 after P183 has elapsed with the start signal being present";
			break;

		case 143:
			msg = "Internal error";
			break;

		case   4:
			msg = "Hardware monitoring\nShort-circuit within the motor or connecting cable (overcurrent, overvoltage, air cooler defective)";
			break;

		case  62:
			msg = "Pump temperature warning\nP127 > P128";
			break;

		default:
			if (error_code > 200)
				msg = "Internal error\nError within the converter or external voltage applied to the inputs: " + PVUtil.getString(pvs[1]);
			else
				msg = "Unknown error code: " + PVUtil.getString(pvs[1]);
			break;

	}

} catch (err)
{
	logger.severe(err);
}

widget.setPropertyValue("tooltip", "$(pv_name)\n" + msg);
