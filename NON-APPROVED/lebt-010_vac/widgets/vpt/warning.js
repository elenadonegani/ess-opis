PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;
ScriptUtil = org.csstudio.display.builder.runtime.script.ScriptUtil;
Logger = org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger();

var warningMsg  = "";
var warningCode = 0;

if (PVUtil.getLong(pvs[0]))
	warningCode = PVUtil.getLong(pvs[1]);

switch (warningCode) {
	case 99:
		warningMsg = "Bypass Interlock Activated";
		break;
	case 98:
		warningMsg = "Pump Starting Prevented by Tripped Interlock";
		break;
	case 5:
		warningMsg = "Pressure Interlock Bypassed";
		break;
	case 4:
		warningMsg = "Hardware Interlock Bypassed";
		break;
	case 3:
		warningMsg = "Software Interlock Bypassed";
		break;

	case 0:
		break;
	default:
		warningMsg = "Warning Code: " + PVUtil.getString(pvs[1]);
		break;
}
Logger.info(pvs[1] + " Warning code: " + warningCode + " message: " + warningMsg);

try {
	pvs[2].setValue(warningMsg);
} catch (err) {
	Logger.warning(err);
}
