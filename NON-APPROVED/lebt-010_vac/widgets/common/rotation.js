PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;
ScriptUtil = org.csstudio.display.builder.runtime.script.ScriptUtil;
Logger = org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger();

var rotation = PVUtil.getString(pvs[0]);

function hide_pipe(pipe) {
	try {
		ScriptUtil.findWidgetByName(widget, "PIPE_" + pipe.toUpperCase()).setPropertyValue("visible", "false");
	} catch (err) {
		Logger.severe("show_pipe("+ pipe.toUpperCase() +"): " + err)
	}
}

if (isNaN(rotation)) {
	rotation = 0;
}

switch (Number(rotation)) {
	case  0:
//		show_pipe("right");
//		show_pipe("below");
		hide_pipe("left");
		break;
	case 90:
//		show_pipe("left");
//		show_pipe("below");
		hide_pipe("right");
		break;
	case 180:
//		show_pipe("left");
		hide_pipe("below");
		hide_pipe("right");
		break;
	case 270:
//		show_pipe("right");
		hide_pipe("left");
		hide_pipe("below");
		break;
	default:
		Logger.severe("ANGLE: " + rotation);
		rotation = 0;
}

try {
	ScriptUtil.findWidgetByName(widget, "Symbol").setPropertyValue("rotation", rotation);
} catch (err) {
	Logger.severe("(rotation.js): No widget with name 'Symbol': " + err);
}
